# Command Line Interface

In order to have access to OpenStack's API, you have to use so-called OpenStack Application Credentials. In short,
it is a form of token-based authentication providing easy and secure access without the use of passwords.

## Getting Credentials
1. In **Identity &gt; Application Credentials**, click on **Create Application Credential**.
2. Choose name, description and expiration date & time.

  ![](/cli/images/app_creds_1.png)

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    Do NOT select specific roles, unless directed otherwise by user support.
  </div>

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    If you decide to select specific roles, you should always include at least the <strong>member</strong> role.
    If you are planning to use the orchestration API, add the <strong>heat_stack_owner</strong> role as well and
    check <strong>Unrestricted</strong>.
  </div>

3. Download provided configuration files for the OpenStack CLI client.

  ![](/cli/images/app_creds_2.png)

## Setting Up
1. [Install](https://pypi.org/project/python-openstackclient/) and
   [configure](https://docs.openstack.org/python-openstackclient/rocky/configuration/index.html)
   OpenStack CLI client.

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(255,0,0,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>WARNING:</strong><br/><br/>
     Add the following line to the <strong>openrc</strong> file:<br/>
     <strong>export OS_VOLUME_API_VERSION=3</strong>
     <br/><br/>
     Add the following line to the <strong>clouds.yaml</strong> file:<br/>
     <strong>volume_api_version: 3</strong>
   </div>

2. Follow the official [Launch instances](https://docs.openstack.org/nova/rocky/user/launch-instances.html) guide.

---

## Creating a key-pair
1. Assuming your ssh public key is stored in `~/.ssh/id_rsa.pub`
```
openstack keypair create --public-key ~/.ssh/id_rsa.pub my-key1
```

## Create security group
1. Create:
```
openstack security group create my-security-group
```

2. Add rules to your security group:
```
openstack security group rule create --description "Permit SSH" --remote-ip 0.0.0.0/0 --protocol tcp --dst-port 22 --ingress my-security-group
openstack security group rule create --description "Permit ICMP (any)" --remote-ip 0.0.0.0/0 --protocol icmp --icmp-type -1 --ingress my-security-group
```

3. Verify:
```
 openstack security group show my-security-group
```

## Create network

1. Create network + subnet (from auto-allocated pool)
```
openstack network create my-net1
openstack subnet create --network my-net1 --subnet-pool private-192-168 my-sub1
```

2. Create router:
```
openstack router create my-router1
```
Current router have no ports, which makes it pretty useless, we need to create at least 2 interfaces (external and internal)

3. Set external network for router (let us say public-muni-147-251-124), and the external port will be created automatically:
```
openstack router set --external-gateway public-muni-147-251-124 my-router1
```

4. Check which IP address is set as gateway for our subnet (default: first address of the subnet):
```
GW_IP=$(openstack subnet show my-sub1 -c gateway_ip -f value)
```

5. Create internal port for router (gateway for the network my-net1):
```
openstack port create --network my-net1 --disable-port-security --fixed-ip ip-address=$GW_IP my-net1-port1-gw
```

6. Add port to the router:
```
openstack router add port my-router1 my-net1-port1-gw
```

## Create volume
<div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>WARNING:</strong><br/>
  Skipping this section can lead to unreversible loss of data
  </div>

Volumes are create automatically when creating an instance in GUI, but we need to create them manually in case of CLI

1. Create bootable volume from image(e.g. centos):
```
openstack volume create --image "centos-7-1809-x86_64" --size 40 my_vol1
```

## Create server

1. Create instance:
```
openstack server create --flavor "standard.small" --volume my_vol1 \
 --key-name my-key1 --security-group my-security-group --network my-net1 my-server1
```

## Assign floating ip address

1. Create and assign floating IP address:
```
FLOAT_IP=$(openstack floating ip create --description my-float1 -c floating_ip_address -f value public-muni-147-251-124)
openstack server add floating ip my-server1 $FLOAT_IP
```

## Full Reference
See [OpenStack CLI Documentation](https://docs.openstack.org/python-openstackclient/rocky/).
