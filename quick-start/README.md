# Quick Start

The following guide will take you through the steps necessary to start your first virtual machine instance.

Prerequisites:
* Up-to-date web browser
* Active account in MetaCentrum Cloud
* Basic knowledge of SSH (for remote connections)

## Sign In

The dashboard is available at [https://dashboard.cloud.muni.cz](https://dashboard.cloud.muni.cz).

1. Select `EINFRA CESNET`.

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    Users of the Czech national e-infrastructure should always select <strong>EINFRA CESNET</strong>.
    International users may choose <strong>EGI Check-in</strong> or <strong>ELIXIR AAI</strong>, depending on
    their membership.
  </div>

2. Click on **Sign In**.

  ![](/quick-start/images/sign_in1.png)

3. Select your institution from the drop-down list.

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Notice:</strong><br/>
     You can use the search box at the top as a filter.
   </div>

4. Provide your institution-specific sign-in credentials.
5. Wait to be redirected back to our dashboard.

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    If your attempts repeatedly result in an error message about projects, make sure you have an active MetaCentrum account
    and that you are selecting the correct institution when attempting to log in. If the problem persists, please, contact user
    support.
  </div>

## Create Key Pair

All virtual machine instances running in the cloud have to be accessed remotely. The most common way of accessing
an instance remotely is SSH. Using SSH requires a pair of keys - a public key and a private key.

1. In **Project &gt; Compute &gt; Key Pairs**, click the **Create Key Pair** button.

  ![](/quick-start/images/CreateKeyPair1.png)

2. Name your key. Avoid using special characters, if possible.

  ![](/quick-start/images/CreateKeyPair2.png)

3. You can see your newly created key pair and its fingerprint. We recommend you store the key in
   a safe location and back it up in case you accidentally delete it.

  ![](/quick-start/images/CreateKeyPair3.png)

For details, refer to [the official documentation](https://docs.openstack.org/horizon/rocky/user/configure-access-and-security-for-instances.html).

## Update Security Group

In MetaCentrum Cloud, all incoming traffic from external networks to virtual machine instances is blocked by default.
You need to explicitly allow access to virtual machine instances and services via a security group.

You need to add two new rules to be able to connect to your new instance (or any instance using the given security group).
This is similar to setting up firewall rules on your router or server. If set up correctly, you will be able to access
your virtual machine via SSH from your local terminal.

1. Go to **Project &gt;  Network &gt; Security Groups**. Click on **Manage Rules**, for the **default** security group.

  ![](/quick-start/images/SecurityGroups1.png)

2. Click on **Add rule**, choose **SSH** and leave the remaining fields unchanged.
   This will allow you to access your instance.

  ![](/quick-start/images/SecurityGroups2.png)

3. Click on **Add rule**, choose **ALL ICMP** and leave the remaining fields unchanged.
   This will allow you to `ping` your instance.

  ![](/quick-start/images/SecurityGroups3.png)

For details, refer to [the official documentation](https://docs.openstack.org/horizon/rocky/user/configure-access-and-security-for-instances.html).

## Create Virtual Machine Instance

1. In **Compute &gt; Instances**, click the **Launch Instance** button.

  ![](/quick-start/images/instance0.png)

2. Choose name, description, and the number of instances.
   If you are creating more instances, `-%i` will be automatically appended to the name of each instance.

  ![](/quick-start/images/instance1.png)

3. Choose an image from which to boot the instance. Image will be automatically copied to a persistent volume
   that will remain available even after the instance has been deleted.

  ![](/quick-start/images/instance2.png)

4. Choose the size of your instance by selecting a flavor. Additional volumes for data can be attached later on.

  ![](/quick-start/images/instance3.png)

5. __(optional)__ On the Configuration tab, you can use `user_data` to customize your instance at boot.
   See [cloud-init](https://cloud-init.io/) for details.

For details, refer to [the official documentation](https://docs.openstack.org/horizon/rocky/user/launch-instances.html).

## Setup Router gateway (Required for Group projects)
Completing [Create Virtual Machine Instance](/quick-start/#create-virtual-machine-instance) created instance connected
to software defined network represented by auto-allocated private network, subnet and router. Router has by default gateway address
from External Network **public-muni-147-251-124**, which is for non-production only and is released daily. For persistent gateway address
follow these steps:

1. In **Network &gt; Routers**, click the **Set Gateway** button next to auto_allocated_router.

2. From list of External Network choose **public-cesnet-78-128-251** or **private-muni-10-16-116**

Router is setup with persistent gateway.

## Associate Floating IP

To make an instance accessible from external networks (e.g., The Internet), a so-called Floating IP Address has to be
associated with it.

1. In **Project &gt; Network &gt; Floating IPs**, select **Allocate IP to Project**. Pick an IP pool from which to allocate
   the address. Click on **Allocate IP**.

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Notice:</strong><br/>
     When picking an IP pool from which to allocate a floating IP address, please, keep in mind that you have to allocate
     an address in the pool connected to your virtual router. For automatically allocated routers, this means <strong>public-muni-147-251-124</strong>.
   </div>

   <div style="border-width:0;border-left:5px solid #ff0000;background-color:rgba(255, 0, 0, 0.2);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Warning:</strong><br/>
     Group projects can persistently allocate IPs only from <strong>public-cesnet-78-128-251</strong> or <strong>private-muni-10-16-116</strong> networks.
     IPs from <strong>public-muni-147-251-124</strong> allocated to Group projects will be released daily.
   </div>

   <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
     <strong>Notice:</strong><br/>
     Please, keep an eye on the number of allocated IPs in <strong>Project &gt; Network &gt; Floating IPs</strong>. IPs
     remain allocated to you until you explicitly release them in this tab. Detaching an IP from an instance is not sufficient
     and the IP in question will remain allocated to you and consume your Floating IP quota.
   </div>

1. In **Project &gt; Compute &gt; Instances**, select **Associate Floating IP** from the **Actions** drop-down menu for the
   given instance.

2. Select IP address and click on **Associate**.

  ![](/quick-start/images/allocate_IP3.png)

## Create Volume

When storing a large amount of data in a virtual machine instance, it is advisable to use a separate volume and not the
root file system containing the operating system. It adds flexibility and often prevents data loss. Volumes can be
attached and detached from instances at any time, their creation and deletion are managed separately from instances.

1. In **Project &gt; Volumes &gt; Volumes**, select **Create Volume**.
2. Provide name, description and size in GBs. If not instructed otherwise, leave all other fields unchanged.
3. Click on **Create Volume**.
4. __(optional)__ In **Project &gt; Compute &gt; Instances**, select **Attach Volume** from the **Actions** drop-down menu for the
   given instance.
5. __(optional)__ Select **Volume ID** from the drop-down list and click **Attach Volume**.

For details, refer to [the official documentation](https://docs.openstack.org/horizon/rocky/user/manage-volumes.html).
