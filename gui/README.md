# Advanced Features

The following guide will introduce you to advanced features available in MetaCentrum Cloud.
For basic instructions on how to start a virtual machine instance, see [Quick Start](/quick-start/README.md).

## Virtual Networks

MetaCentrum Cloud offers software-defined networking as one of its services. Users have the ability to create their own
networks and subnets, connect them with routers, and set up tiered network topologies.

Prerequisites:
* Basic understanding of routing
* Basic understanding of TCP/IP

For details, refer to [the official documentation](https://docs.openstack.org/horizon/rocky/user/create-networks.html).

### Create Network

1. Go to **Project &gt; Network &gt; Networks**, click on **Create Network**.

2. Choose name and click **Next**.

3. In the subnet tab, choose a subnet name. In **Network Address Source**, select **Allocate Network Addres from a pool**.
   In **Address pool** select any of the available pools. Click **Next**.

4. Click **Create**. Do not change any other options.

5. Go to **Project &gt; Network &gt; Network Topology**, review your newly created network topology.

  ![](/gui/images/network5.png)

### Create Router

1. Go to **Project &gt; Network &gt; Routers**, click on the **Create Router** button.

2. Choose a name. Select **External Network** and click **Create Router**.

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    Please, remember that your will have to allocate floating IP addresses in the selected External Network for all instances
    using this router as a gateway.
  </div>

3. Go to **Project &gt; Network &gt; Network Topology**, the newly create router should be now present.

  ![](/gui/images/router3.png)

4. Click on the router icon, select **Add Interface**.

  ![](/gui/images/router4.png)

5. Choose the previously created network/subnet from the drop-down menu. Click **Submit**.

  ![](/gui/images/router5.png)

6. The router is now attached to an external network.

  ![](/gui/images/router6.png)

  <div style="border-width:0;border-left:5px solid #b8d6f4;background-color:rgba(228,240,251,0.3);margin:20px 0;padding:10px 20px;font-size:15px;">
    <strong>Notice:</strong><br/>
    Routers can also be used to route traffic between internal networks. This is an advanced topic not covered in this guide.
  </div>

## Orchestration

The OpenStack orchestration service can be used to deploy and manage complex virtual topologies as single entities,
including basic auto-scaling and self-healing.

For details, refer to [the official documentation](https://docs.openstack.org/heat-dashboard/rocky/user/index.html).

## Image upload

We don't support uploading own images by default. MetaCentrum Cloud images are optimized for running in the cloud and we recommend users
to customize them instead of building own images from scratch. If you need upload custom image, please contact user support for appropriate permissions.

Instructions for uploading custom image:

1. Upload only images in RAW format (not qcow2, vmdk, etc.).

2. Upload is supported only through OpenStack [CLI](https://cloud.gitlab-pages.ics.muni.cz/documentation/cli/) with Application Credentials.

3. Each image needs to contain metadata:
```
hw_scsi_model=virtio-scsi
hw_disk_bus=scsi
hw_rng_model=virtio
hw_qemu_guest_agent=yes
os_require_quiesce=yes
```
Following needs to be setup correctly (consult official [documentation](https://docs.openstack.org/glance/rocky/admin/useful-image-properties.html#image-property-keys-and-values))
or instances won't start:
```
os_type=linux # example
os_distro=ubuntu # example
```

4. Images should contain cloud-init, qemu-guest-agent and grow-part tools

5. OpenStack will resize instance after start. Image shouldn't contain any empty partitions or free space

## Add SWAP file to instance

By default VMs after creation do not have SWAP partition. If you need to add a SWAP file to your system you can download and run [script](https://gitlab.ics.muni.cz/cloud/cloud-tools/blob/master/swap.sh) that create SWAP file on your VM.
